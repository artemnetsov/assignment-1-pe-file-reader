/// @file
/// @brief File containing function for "prety print".

#include "pprint.h"
#include "pe.h"
#include <stdio.h>

/// Placeholder for uint64_t for different compilers.
#if defined _MSC_VER
#define UINT64_T_PLACEHOLDER "%llu"
#else
#define UINT64_T_PLACEHOLDER "%lu"
#endif

/// @brief Prints main header of PE file to stdout.
/// @param[in] header Pointer to an instace of PE main header.
void pprint_pe_headers(struct ImageHeader* header)
{
	printf("\nheaders:\n");
	// PE\0\0
	printf("\tmagic: %c%cx%xx%x\n", header->magic[0], header->magic[1], header->magic[2], header->magic[3]);
	printf("\tmachine: %u\n", header->machine);
	printf("\tnumber of sections: %u\n", header->numberOfSections);
	printf("\ttimestamp: %u\n", header->timeDateStamp);
	printf("\tpointer to symbol table: %u\n", header->pointerToSymbolTable);
	printf("\tnumber of symbols: %u\n", header->numberOfSymbols);
	printf("\tsize of optional header: %u\n", header->sizeOfOptionalHeader);
	printf("\tcharacteristics: %u\n", header->characteristics);
}

/// @brief Prints optional header of PE file to stdout.
/// @param[in] optional_header Pointer to an instace of PE optional header.
void pprint_optional_headers(struct ImageOptionalHeader* optional_header)
{
	printf("\noptional headers:\n");
	printf("\tmagic: x%x\n", optional_header->magic);
	printf("\tmajor linker version: %u\n", optional_header->majorLinkerVersion);
	printf("\tminor linker version: %u\n", optional_header->minorLinkerVersion);
	printf("\tsize of code: %u\n", optional_header->sizeOfCode);
	printf("\tsize of initialized data: %u\n", optional_header->sizeOfInitializedData);
	printf("\tsize of uninitialized data: %u\n", optional_header->sizeOfUninitializedData);
	printf("\taddress of entry point: %u\n", optional_header->addressOfEntryPoint);
	printf("\tbase of code: %u\n", optional_header->baseOfCode);
	printf("\tbase of data: %u\n", optional_header->baseOfData);
	printf("\timage base: "UINT64_T_PLACEHOLDER"\n", optional_header->imageBase);
	printf("\tsection alignment: %u\n", optional_header->sectionAlignment);
	printf("\tfile alignment: %u\n", optional_header->fileAlignment);
	printf("\tmajor operating system version: %u\n", optional_header->majorOperatingSystemVersion);
	printf("\tminor operating system version: %u\n", optional_header->minorOperatingSystemVersion);
	printf("\tmajor image version: %u\n", optional_header->majorImageVersion);
	printf("\tminor image version: %u\n", optional_header->minorImageVersion);
	printf("\tmajor subsystem version: %u\n", optional_header->majorSubsystemVersion);
	printf("\tminor subsystem version: %u\n", optional_header->minorSubsystemVersion);
	printf("\twin32 version value: %u\n", optional_header->win32VersionValue);
	printf("\tsize of image: %u\n", optional_header->sizeOfImage);
	printf("\tsize of headers: %u\n", optional_header->sizeOfHeaders);
	printf("\tchecksum: %u\n", optional_header->checkSum);
	printf("\tsubsystem: %u\n", optional_header->subsystem);
	printf("\tdll characteristics: %u\n", optional_header->dllCharacteristics);
	printf("\tsize of stack reserve: "UINT64_T_PLACEHOLDER"\n", optional_header->sizeOfStackReserve);
	printf("\tsize of stack commit: "UINT64_T_PLACEHOLDER"\n", optional_header->sizeOfStackCommit);
	printf("\tsize of heap reserve: "UINT64_T_PLACEHOLDER"\n", optional_header->sizeOfHeapReserve);
	printf("\tsize of heap commit: "UINT64_T_PLACEHOLDER"\n", optional_header->sizeOfHeapCommit);
	printf("\tloader flags: %u\n", optional_header->loaderFlags);
	printf("\tnumber of rva and sizes: %u\n", optional_header->numberOfRvaAndSizes);
	printf("\texport table: "UINT64_T_PLACEHOLDER"\n", optional_header->exportTable);
	printf("\timport table: "UINT64_T_PLACEHOLDER"\n", optional_header->importTable);
	printf("\tresource table: "UINT64_T_PLACEHOLDER"\n", optional_header->resourceTable);
	printf("\texception table: "UINT64_T_PLACEHOLDER"\n", optional_header->exceptionTable);
	printf("\tcertificate table: "UINT64_T_PLACEHOLDER"\n", optional_header->certificateTable);
	printf("\tbase relocation table: "UINT64_T_PLACEHOLDER"\n", optional_header->baseRelocationTable);
	printf("\tdebug: "UINT64_T_PLACEHOLDER"\n", optional_header->debug);
	printf("\tarchitecture: "UINT64_T_PLACEHOLDER"\n", optional_header->architecture);
	printf("\tglobal ptr: "UINT64_T_PLACEHOLDER"\n", optional_header->globalPtr);
	printf("\ttls table: "UINT64_T_PLACEHOLDER"\n", optional_header->tlsTable);
	printf("\tload config table: "UINT64_T_PLACEHOLDER"\n", optional_header->loadConfigTable);
	printf("\tbound table: "UINT64_T_PLACEHOLDER"\n", optional_header->boundTable);
	printf("\tiat: "UINT64_T_PLACEHOLDER"\n", optional_header->iat);
	printf("\tdelay import descriptor: "UINT64_T_PLACEHOLDER"\n", optional_header->delayImportDescriptor);
	printf("\tclr runtime header: "UINT64_T_PLACEHOLDER"\n", optional_header->clrRuntimeHeader);
	printf("\treserved: "UINT64_T_PLACEHOLDER"\n", optional_header->reserved);
}

/// @brief Prints section header of PE file to stdout.
/// @param[in] section_header Pointer to an instace of PE section header.
void pprint_section_headers(struct ImageSectionHeader* section_header)
{
	printf("\nsection headers:\n");
	printf("\tname: ");
	for (int i = 0; i < sizeof(section_header->name); i++)
	{
		printf("%c", section_header->name[i]);
	}
	printf("\n\tvirtual size: %u\n", section_header->virtualSize);
	printf("\tvirtual address: %u\n", section_header->virtualAddress);
	printf("\tsize of raw data: %u\n", section_header->sizeOfRawData);
	printf("\tpointer to raw data: %u\n", section_header->pointerToRawData);
	printf("\tpointer to relocations: %u\n", section_header->pointerToRelocations);
	printf("\tpointer to linenumbers: %u\n", section_header->pointerToLinenumbers);
	printf("\tnumber of relocations: %u\n", section_header->numberOfLinenumbers);
	printf("\tnumber of linunumbers: %u\n", section_header->numberOfRelocations);
	printf("\tcharacteristics: %u\n", section_header->characteristics);
}

/// @brief Prints the content of PE file to stdout.
/// @param[in] pe Pointer to an instace of PE file.
void pprint_pe_file(struct PEFile* pe)
{
	printf("pe file:\n");
	pprint_pe_headers(&pe->header);
	pprint_optional_headers(&pe->optionalHeader);
	for (int i = 0; i < pe->header.numberOfSections; i++)
	{
		pprint_section_headers(&pe->sectionHeaders[i]);
	}
}
