/// @file 
/// @brief File containg headers for function for "prety print"

#include "pe.h"

void pprint_pe_headers(struct ImageHeader* header);
void pprint_optional_headers(struct ImageOptionalHeader* optional_header);
void pprint_section_headers(struct ImageSectionHeader* section_header);
void pprint_pe_file(struct PEFile* pe);
