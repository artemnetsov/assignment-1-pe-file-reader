/// @file 
/// @brief Main application file.

#include "pprint.h"
#include "pe.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// Application name string.
#define APP_NAME "section-extractor"

/// Main offset in PE-files where an offset of PE header is stored.
#define MAIN_OFFSET 0x3c

/// Error message that tells that memory can not be allocated.
#define CANNOT_ALLOCATE_MEMORY_ERROR_MESSAGE "oopsie-woopsie we can't allocate memory...\n"
/// Error message that tells that the file can not be opened.
#define CANNOT_OPEN_FILE_ERROR_MESSAGE "oopsie-woopsie we can't open file...\n"
/// Error message that tells that section can not be found.
#define CANNOT_FIND_SECTION_ERROR_MESSAGE "oopsie-woopsie we can't find this section...\n"
/// Error message for general errors.
#define GENERAL_ERROR_MESSAGE "oopsie-woopsie something went wrong...\n"

/// @brief Parses PE file to a PEFile structure.
/// @param[in] file PE file that should be parsed.
/// @param[in] pe PEFile structure that should have the content of PE file.
/// @return 0 in case of success or -1 if the file cannot be parsed.
int8_t parse_pe_file(FILE* file, struct PEFile* pe)
{
	fseek(file, MAIN_OFFSET, SEEK_SET);

	uint32_t header_offset;
	fread(&header_offset, sizeof(uint32_t), 1, file);

	struct ImageHeader header;
	fseek(file, header_offset, SEEK_SET);
	fread(&header, sizeof(header), 1, file);

	uint32_t optional_header_offset = header_offset + sizeof(header);

	struct ImageOptionalHeader optional_header;
	fseek(file, optional_header_offset, SEEK_SET);
	fread(&optional_header, header.sizeOfOptionalHeader, 1, file);

	struct ImageSectionHeader* section_headers = (struct ImageSectionHeader*)malloc(sizeof(struct ImageSectionHeader) * header.numberOfSections);
	if (!section_headers)
	{
		perror(CANNOT_ALLOCATE_MEMORY_ERROR_MESSAGE);
		return -1;
	}

	uint32_t section_header_offset = optional_header_offset + header.sizeOfOptionalHeader;

	struct ImageSectionHeader section_header;
	for (uint32_t section_index = 0; section_index < header.numberOfSections; section_index++)
	{
		fseek(file, section_header_offset, SEEK_SET);
		fread(&section_header, sizeof(struct ImageSectionHeader), 1, file);

		section_headers[section_index] = section_header;

		section_header_offset += sizeof(struct ImageSectionHeader);
	}

	pe->header = header;
	pe->optionalHeader = optional_header;
	pe->sectionHeaders = section_headers;

	return 0;
}

/// @brief Extracts section in PE file and writes it to a separate file.
/// @param[in] section_name Number of section if Pe file.
/// @param[in] input PE file where to find section.
/// @param[in] output File that should containt extracted section.
/// @return 0 in case of success or -1 if the section cannot be extracted.
int8_t extract_section_to_file(char section_name[], FILE* input, FILE* output)
{
	struct PEFile pe;

	int8_t flag = parse_pe_file(input, &pe);
	if (flag)
		return -1;

	for (int section_index = 0; section_index < pe.header.numberOfSections; section_index++)
	{
		if (strcmp(pe.sectionHeaders[section_index].name, section_name) != 0)
			continue;

		uint32_t section_offset = pe.sectionHeaders[section_index].pointerToRawData;
		uint32_t section_size = pe.sectionHeaders[section_index].sizeOfRawData;

		fseek(input, section_offset, SEEK_SET);

		uint8_t* result = (uint8_t*)malloc(sizeof(uint8_t) * section_size);
		if (!result)
		{
			free(pe.sectionHeaders);
			perror(CANNOT_ALLOCATE_MEMORY_ERROR_MESSAGE);
			return -1;
		}

		fread(result, sizeof(uint8_t) * section_size, 1, input);
		fwrite(result, sizeof(uint8_t) * section_size, 1, output);

		free(result);
		free(pe.sectionHeaders);

		return 0;
	}

	free(pe.sectionHeaders);
	perror(CANNOT_FIND_SECTION_ERROR_MESSAGE);
	return -1;
}

/// @brief Application entry point.
/// @param[in] argc Number of command line arguments.
/// @param[in] argv Command line arguments.
/// @return 0 in case of success or error code.
int main(int argc, char** argv)
{
	if (argc < 4)
	{
		perror("Error: two few command line arguments!");
		(void)getchar();	// supressing wargnings
		return -1;
	}

	FILE* input = fopen(argv[1], "r");
	FILE* output = fopen(argv[3], "w");

	if (!input || !output)
	{
		perror(CANNOT_OPEN_FILE_ERROR_MESSAGE);
		(void)getchar();
		return -1;
	}
	
	int8_t extraction_result = extract_section_to_file(argv[2], input, output);
	if (extraction_result)
	{
		(void)getchar();
		return -1;
	}

	fclose(input);
	fclose(output);

	(void)getchar();
	return 0;
}
