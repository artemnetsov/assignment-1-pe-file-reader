/// @file 
/// @brief File containing structure for PE file.

#include <stdint.h>

#pragma once

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

/// Structure containing PE main header file.
#ifdef _MSC_VER
struct ImageHeader
#else
struct __attribute__((__packed__)) ImageHeader
#endif
{
	/// @name Magic number.
	///@{

	/// Magic number that indicates beginning of PE main header: PE\0\0.
	uint8_t magic[4];

	///@}

	/// @name PE header fields.
	///@{

	/// The number that identifies the type of target machine.
	uint16_t machine;
	/// The number of sections. This indicates the size of the section table, which immediately follows the headers.
	uint16_t numberOfSections;
	/// The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created.
	uint32_t timeDateStamp;
	/// The file offset of the COFF symbol table, or zero if no COFF symbol table is present. This value should be zero for an image because COFF debugging information is deprecated.
	uint32_t pointerToSymbolTable;
	/// The number of entries in the symbol table. This data can be used to locate the string table, which immediately follows the symbol table. This value should be zero for an image because COFF debugging information is deprecated.
	uint32_t numberOfSymbols;
	/// The size of the optional header, which is required for executable files but not for object files. This value should be zero for an object file. 
	uint16_t sizeOfOptionalHeader;
	/// The flags that indicate the attributes of the file.
	uint16_t characteristics;

	///@}
};

/// Structure containing PE optional header file.
#ifdef _MSC_VER
struct ImageOptionalHeader
#else
struct __attribute__((__packed__)) ImageOptionalHeader
#endif
{
	/// @name Magic number.
	///@{


	/// The unsigned integer that identifies the state of the image file.
	uint16_t magic;

	///@}

	/// @name Optional header fields.
	///@{

	/// The linker major version number.
	uint8_t majorLinkerVersion;
	/// The linker minor version number.
	uint8_t minorLinkerVersion;
	/// The size of the code (text) section, or the sum of all code sections if there are multiple sections.
	uint32_t sizeOfCode;
	/// The size of the initialized data section, or the sum of all such sections if there are multiple data sections.
	uint32_t sizeOfInitializedData;
	/// The size of the uninitialized data section (BSS), or the sum of all such sections if there are multiple BSS sections.
	uint32_t sizeOfUninitializedData;
	/// The address of the entry point relative to the image base when the executable file is loaded into memory.
	uint32_t addressOfEntryPoint;
	/// The address that is relative to the image base of the beginning-of-code section when it is loaded into memory.
	uint32_t baseOfCode;
	/// The address that is relative to the image base of the beginning-of-data section when it is loaded into memory.
	uint32_t baseOfData;
	/// The preferred address of the first byte of image when loaded into memory.
	uint64_t imageBase;
	/// The alignment (in bytes) of sections when they are loaded into memory.
	uint32_t sectionAlignment;
	/// The alignment factor (in bytes) that is used to align the raw data of sections in the image file.
	uint32_t fileAlignment;
	/// The major version number of the required operating system.
	uint16_t majorOperatingSystemVersion;
	/// The minor version number of the required operating system.
	uint16_t minorOperatingSystemVersion;
	/// The major version number of the image.
	uint16_t majorImageVersion;
	/// The minor version number of the image.
	uint16_t minorImageVersion;
	/// The major version number of the subsystem.
	uint16_t majorSubsystemVersion;
	/// The minor version number of the subsystem.
	uint16_t minorSubsystemVersion;
	/// Reserved, must be zero.
	uint32_t win32VersionValue;
	/// The size (in bytes) of the image, including all headers, as the image is loaded in memory.
	uint32_t sizeOfImage;
	/// The combined size of an MS-DOS stub, PE header, and section headers rounded up to a multiple of FileAlignment.
	uint32_t sizeOfHeaders;
	/// The image file checksum.
	uint32_t checkSum;
	/// The subsystem that is required to run this image.
	uint16_t subsystem;
	/// Characteristics for DLL.
	uint16_t dllCharacteristics;
	/// The size of the stack to reserve.
	uint64_t sizeOfStackReserve;
	/// The size of the stack to commit.
	uint64_t sizeOfStackCommit;
	/// The size of the local heap space to reserve.
	uint64_t sizeOfHeapReserve;
	/// The size of the local heap space to commit.
	uint64_t sizeOfHeapCommit;
	/// Reserved, must be zero.
	uint32_t loaderFlags;
	/// The number of data-directory entries in the remainder of the optional header.
	uint32_t numberOfRvaAndSizes;
	/// The export table address and size.
	uint64_t exportTable;
	/// The import table address and size.	
	uint64_t importTable;
	/// The resource table address and size.
	uint64_t resourceTable;
	/// The exception table address and size.
	uint64_t exceptionTable;
	/// The attribute certificate table address and size.
	uint64_t certificateTable;
	/// The base relocation table address and size.
	uint64_t baseRelocationTable;
	/// The debug data starting address and size.
	uint64_t debug;
	/// Reserved, must be 0.
	uint64_t architecture;
	/// The RVA of the value to be stored in the global pointer register. The size member of this structure must be set to zero.
	uint64_t globalPtr;
	/// The thread local storage (TLS) table address and size.
	uint64_t tlsTable;
	/// The load configuration table address and size.
	uint64_t loadConfigTable;
	/// The bound import table address and size.
	uint64_t boundTable;
	/// The import address table address and size.
	uint64_t iat;
	/// The delay import descriptor address and size.
	uint64_t delayImportDescriptor;
	/// The CLR runtime header address and size.
	uint64_t clrRuntimeHeader;
	/// Reserved field, must be 0.
	uint64_t reserved;

	///@}
};

/// Structure containing PE section header file.
#ifdef _MSC_VER
struct ImageSectionHeader
#else
struct __attribute__((__packed__)) ImageSectionHeader
#endif
{
	/// Name of the section.
	char name[8];
	/// The total size of the section when loaded into memory.
	uint32_t virtualSize;
	/// Tthe address of the first byte of the section relative to the image base when the section is loaded into memory.
	uint32_t virtualAddress;
	/// The size of the section (for object files) or the size of the initialized data on disk (for image files). 
	uint32_t sizeOfRawData;
	/// The file pointer to the first page of the section within the PE file.
	uint32_t pointerToRawData;
	/// The file pointer to the beginning of relocation entries for the section.
	uint32_t pointerToRelocations;
	/// The file pointer to the beginning of line-number entries for the section.
	uint32_t pointerToLinenumbers;
	/// The number of relocation entries for the section. This is set to zero for executable images.
	uint16_t numberOfRelocations;
	/// The number of line-number entries for the section.
	uint16_t numberOfLinenumbers;
	/// The flags that describe the characteristics of the section.
	uint32_t characteristics;
};

/// Structure containing PE file.
#ifdef _MSC_VER
struct PEFile
#else
struct __attribute__((__packed__)) PEFile
#endif
{
	/// Main header for PE file.
	struct ImageHeader header;
	/// Optional header for PE file.
	struct ImageOptionalHeader optionalHeader;
	/// Section headers for PE file.
	struct ImageSectionHeader* sectionHeaders;
};

#ifdef _MSC_VER
#pragma pack(pop)
#endif
